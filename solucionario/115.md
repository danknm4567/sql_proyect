# 115. Disparador (eliminar)
## Ejercicios propuestos

Un comercio almacena los datos de los artículos que tiene para la venta en una tabla denominada "articulos". En otra tabla denominada "ventas" almacena el código de cada artículo, la cantidad que se vende y la fecha.

1. Elimine las tablas:

```sql
drop table ventas;
drop table articulos;
```

2. Cree las tablas con las siguientes estructuras:

```sql
create table articulos(
    codigo number(4) not null,
    descripcion varchar2(40),
    precio number (6,2),
    stock number(4),
    constraint PK_articulos_codigo
    primary key (codigo)
);

create table ventas(
    codigo number(4),
    cantidad number(4),
    fecha date,
    constraint FK_ventas_articulos
    foreign key (codigo)
    references articulos(codigo)
);
```

3. Cree una secuencia llamada "sec_codigoart", estableciendo que comience en 1, sus valores estén entre 1 y 9999 y se incrementen en 1. Antes elimínela por si existe.

**Solución**
```sql
DROP SEQUENCE sec_codigoart;

CREATE SEQUENCE sec_codigoart
START WITH 1
INCREMENT BY 1
MAXVALUE 9999
NOCYCLE;

```
4. Active el paquete para permitir mostrar salida en pantalla.

**Solución**
```sql
BEGIN
  DBMS_OUTPUT.ENABLE;
END;
/

```
5. Cree un trigger que coloque el siguiente valor de una secuencia para el código de "articulos" cada vez que se ingrese un nuevo artículo.
Podemos ingresar un nuevo registro en "articulos" sin incluir el código porque lo ingresará el disparador luego de calcularlo. Si al ingresar un registro en "articulos" incluimos un valor para código, será ignorado y reemplazado por el valor calculado por el disparador.

**Solución**
```sql
CREATE OR REPLACE TRIGGER tr_generar_codigoart
BEFORE INSERT ON articulos
FOR EACH ROW
BEGIN
  :NEW.codigo := sec_codigoart.NEXTVAL;
END;
/

```
6. Ingrese algunos registros en "articulos" sin incluir el código:

```sql
insert into articulos (descripcion, precio, stock) values ('cuaderno rayado 24h',4.5,100);
insert into articulos (descripcion, precio, stock) values ('cuaderno liso 12h',3.5,150);
insert into articulos (descripcion, precio, stock) values ('lapices color x6',8.4,60);
```

7. Recupere todos los artículos para ver cómo se almacenó el código

**Solución**
```sql
SELECT * FROM articulos;

```
8. Ingrese algunos registros en "articulos" incluyendo el código:

```sql
insert into articulos values(160,'regla 20cm.',6.5,40);
insert into articulos values(173,'compas metal',14,35);
insert into articulos values(234,'goma lapiz',0.95,200);
```

9. Recupere todos los artículos para ver cómo se almacenó los códigos
Ignora los códigos especificados ingresando el siguiente de la secuencia.
**Solución**
```sql
SELECT * FROM articulos;

```
10. Cuando se ingresa un registro en "ventas", se debe:

- controlar que el código del artículo exista en "articulos" (lo hacemos con la restricción "foreign key" establecida en "ventas");

- controlar que exista stock, lo cual no puede controlarse con una restricción "foreign key" porque el campo "stock" no es clave primaria en la tabla "articulos"; cree un trigger. Si existe stock, debe disminuirse en "articulos".

Cree un trigger a nivel de fila sobre la tabla "ventas" para el evento se inserción. Cada vez que se realiza un "insert" sobre "ventas", el disparador se ejecuta. El disparador controla que la cantidad que se intenta vender sea menor o igual al stock del articulo y actualiza el campo "stock" de "articulos", restando al valor anterior la cantidad vendida. Si la cantidad supera el stock, debe producirse un error, revertirse la acción y mostrar un mensaje

**Solución**
```sql
CREATE OR REPLACE TRIGGER tr_controlar_stock
BEFORE INSERT ON ventas
FOR EACH ROW
DECLARE
  v_stock NUMBER(4);
BEGIN
  SELECT stock INTO v_stock FROM articulos WHERE codigo = :NEW.codigo;

  IF v_stock >= :NEW.cantidad THEN
    UPDATE articulos SET stock = stock - :NEW.cantidad WHERE codigo = :NEW.codigo;
  ELSE
    RAISE_APPLICATION_ERROR(-20001, 'No hay suficiente stock para realizar la venta.');
  END IF;
END;
/

```
11. Ingrese un registro en "ventas" cuyo código no exista en "articulos"
Aparece un mensaje de error, porque el código no existe. El trigger se ejecutó.

**Solución**
```sql
INSERT INTO ventas VALUES (9999, 10, SYSDATE);

```
12. Verifique que no se ha agregado ningún registro en "ventas"

**Solución**
```sql
SELECT * FROM ventas;

```
13. Ingrese un registro en "ventas" cuyo código exista en "articulos" y del cual haya suficiente stock
Note que el trigger se disparó, aparece el texto "tr_insertar_ventas activado".

**Solución**
```sql
INSERT INTO ventas VALUES (160, 3, SYSDATE);

```
14. Verifique que el trigger se disparó consultando la tabla "articulos" (debe haberse disminuido el stock) y se agregó un registro en "ventas"

**Solución**
```sql
SELECT * FROM articulos;
SELECT * FROM ventas;

```
15. Ingrese un registro en "ventas" cuyo código exista en "articulos" y del cual NO haya suficiente stock
Aparece el mensaje mensaje de error 20001 y el texto que muestra que se disparó el trigger.

**Solución**
```sql
INSERT INTO ventas VALUES (160, 50, SYSDATE);

```
16. Verifique que NO se ha disminuido el stock en "articulos" ni se ha agregado un registro en "ventas"

**Solución**
```sql
SELECT * FROM articulos;
SELECT * FROM ventas;

```
17. El comercio quiere que se realicen las ventas de lunes a viernes de 8 a 18 hs. Reemplace el trigger creado anteriormente "tr_insertar_ventas" para que No permita que se realicen ventas fuera de los días y horarios especificados y muestre un mensaje de error

**Solución**
```sql
CREATE OR REPLACE TRIGGER tr_controlar_ventas
BEFORE INSERT ON ventas
FOR EACH ROW
DECLARE
  v_day VARCHAR2(20);
  v_hour NUMBER(2);
BEGIN
  SELECT TO_CHAR(SYSDATE, 'DAY') INTO v_day FROM DUAL;
  SELECT TO_NUMBER(TO_CHAR(SYSDATE, 'HH24')) INTO v_hour FROM DUAL;

  IF v_day IN ('MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY') AND v_hour BETWEEN 8 AND 18 THEN
    NULL; -- No hay restricciones, se permite la venta
  ELSE
    RAISE_APPLICATION_ERROR(-20002, 'Las ventas solo están permitidas de lunes a viernes de 8 a 18 hs.');
  END IF;
END;
/

```
18. Ingrese un registro en "ventas", un día y horario permitido, si es necesario, modifique la fecha y la hora del sistema

**Solución**
```sql
INSERT INTO ventas VALUES (160, 2, TO_DATE('2023-06-01 10:00:00', 'YYYY-MM-DD HH24:MI:SS'));

```
19. Verifique que se ha agregado un registro en "ventas" y se ha disminuido el stock en "articulos"

**Solución**
```sql
SELECT * FROM articulos;
SELECT * FROM ventas;

```
20. Ingrese un registro en "ventas", un día permitido fuera del horario permitido (si es necesario, modifique la fecha y hora del sistema)
Se muestra un mensaje de error.

**Solución**
```sql
INSERT INTO ventas VALUES (160, 1, TO_DATE('2023-06-01 19:00:00', 'YYYY-MM-DD HH24:MI:SS'));

```
21. Ingrese un registro en "ventas", un día sábado a las 15 hs.

**Solución**
```sql
INSERT INTO ventas VALUES (160, 1, TO_DATE('2023-06-03 15:00:00', 'YYYY-MM-DD HH24:MI:SS'));

```
22. El comercio quiere que los registros de la tabla "articulos" puedan ser ingresados, modificados y/o eliminados únicamente los sábados de 8 a 12 hs. Cree un trigger "tr_articulos" que No permita que se realicen inserciones, actualizaciones ni eliminaciones en "articulos" fuera del horario especificado los días sábados, mostrando un mensaje de error. Recuerde que al ingresar un registro en "ventas", se actualiza el "stock" en "articulos"; el trigger debe permitir las actualizaciones del campo "stock" en "articulos" de lunes a viernes de 8 a 18 hs. (horario de ventas)

**Solución**
```sql
CREATE OR REPLACE TRIGGER tr_controlar_articulos
BEFORE INSERT OR UPDATE OR DELETE ON articulos
FOR EACH ROW
DECLARE
  v_day VARCHAR2(20);
  v_hour NUMBER(2);
BEGIN
  SELECT TO_CHAR(SYSDATE, 'DAY') INTO v_day FROM DUAL;
  SELECT TO_NUMBER(TO_CHAR(SYSDATE, 'HH24')) INTO v_hour FROM DUAL;

  IF v_day = 'SATURDAY' AND v_hour BETWEEN 8 AND 12 THEN
    NULL; -- No hay restricciones, se permite la operación en "articulos" los sábados de 8 a 12 hs
  ELSIF v_day IN ('MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY') AND v_hour BETWEEN 8 AND 18 THEN
    IF UPDATING('STOCK') THEN
      NULL; -- Se permite la actualización del campo "stock" en "articulos" de lunes a viernes de 8 a 18 hs
    ELSE
      RAISE_APPLICATION_ERROR(-20003, 'Las operaciones en la tabla "articulos" solo están permitidas los sábados de 8 a 12 hs.');
    END IF;
  ELSE
    RAISE_APPLICATION_ERROR(-20004, 'Las operaciones en la tabla "articulos" solo están permitidas los sábados de 8 a 12 hs.');
  END IF;
END;
/

```
23. Ingrese un nuevo artículo un sábado a las 9 AM
Note que se activan 2 triggers.

**Solución**
```sql
INSERT INTO articulos VALUES (161, 'Nuevo artículo', 10.5, 50);

```
24. Elimine un artículo, un sábado a las 16 hs.
Mensaje de error.

**Solución**
```sql
DELETE FROM articulos WHERE codigo = 161;

```
25. Actualice el precio de un artículo, un domingo

**Solución**
```sql
UPDATE articulos SET precio = 9.9 WHERE codigo = 160;

```
26. Actualice el precio de un artículo, un lunes en horario de ventas
Mensaje de error.

**Solución**
```sql
UPDATE articulos SET precio = 9.9 WHERE codigo = 160;

```
27. Ingrese un registro en "ventas" que modifique el "stock" en "articulos", un martes entre las 8 y 18 hs.
Note que se activan 2 triggers.

**Solución**
```sql
INSERT INTO ventas VALUES (160, 2, TO_DATE('2023-06-06 12:00:00', 'YYYY-MM-DD HH24:MI:SS'));

```
28. Consulte el diccionario "user_triggers" para ver cuántos trigger están asociados a "articulos" y a "ventas" (3 triggers)

**Solución**
```sql
SELECT COUNT(*) FROM user_triggers WHERE table_name = 'ARTICULOS';
SELECT COUNT(*) FROM user_triggers WHERE table_name = 'VENTAS';

```
29. Elimine el trigger asociado a "ventas"

**Solución**
```sql
DROP TRIGGER tr_controlar_ventas;

```
30. Elimine las tablas "ventas" y "articulos"

**Solución**
```sql
DROP TABLE ventas;
DROP TABLE articulos;

```
31. Consulte el diccionario "user_triggers" para verificar que al eliminar la tabla "articulos" se han eliminado todos los triggers asociados a ella

**Solución**
```sql
SELECT COUNT(*) FROM user_triggers WHERE table_name = 'ARTICULOS';

```