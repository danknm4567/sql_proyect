# 34. Secuencias (create sequence - currval - nextval - drop sequence)

## Ejercicios propuestos

## Ejercicio 01

Una empresa registra los datos de sus empleados en una tabla llamada "empleados".

### 1. Elimine la tabla "empleados":

```sql
drop table empleados;
```

### 2. Cree la tabla:

```sql
create table empleados(
    legajo number(3),
    documento char(8) not null,
    nombre varchar2(30) not null,
    primary key(legajo)
);
```

### 3. Elimine la secuencia "sec_legajoempleados" y luego créela estableciendo el valor mínimo (1), máximo (999), valor inicial (100), valor de incremento (2) y no circular. Finalmente inicialice la secuencia.
**solucion**
```sql
-- Eliminar la secuencia existente
DROP SEQUENCE sec_legajoempleados;

-- Crear la nueva secuencia
CREATE SEQUENCE sec_legajoempleados
    MINVALUE 1
    MAXVALUE 999
    START WITH 100
    INCREMENT BY 2
    NOCYCLE;

-- Inicializar la secuencia
SELECT sec_legajoempleados.nextval FROM dual;
```
### 4. Ingrese algunos registros, empleando la secuencia creada para los valores de la clave primaria:

```sql
insert into empleados
values (sec_legajoempleados.currval,'22333444','Ana Acosta');

insert into empleados
values (sec_legajoempleados.nextval,'23444555','Betina Bustamante');

insert into empleados
values (sec_legajoempleados.nextval,'24555666','Carlos Caseros');

insert into empleados
values (sec_legajoempleados.nextval,'25666777','Diana Dominguez');

insert into empleados
values (sec_legajoempleados.nextval,'26777888','Estela Esper');
```

### 5. Recupere los registros de "libros" para ver los valores de clave primaria.
**solucion**
```sql
SELECT * FROM empleados;

```

### 6. Vea el valor actual de la secuencia empleando la tabla "dual". Retorna 108.
**solucion**
```sql
SELECT sec_legajoempleados.currval FROM dual;

```
### 7. Recupere el valor siguiente de la secuencia empleando la tabla "dual" Retorna 110.
**solucion**
```sql
SELECT sec_legajoempleados.nextval FROM dual;

```
### 8. Ingrese un nuevo empleado (recuerde que la secuencia ya tiene el próximo valor, emplee "currval" para almacenar el valor de legajo)
**solucion**
```sql
INSERT INTO empleados
VALUES (sec_legajoempleados.currval, '29000111', 'Hector Huerta');

```
### 9. Recupere los registros de "libros" para ver el valor de clave primaria ingresado anteriormente.
**solucion**
```sql
SELECT * FROM empleados;

```
### 10. Incremente el valor de la secuencia empleando la tabla "dual" (retorna 112)
**solucion**
```sql
SELECT sec_legajoempleados.nextval FROM dual;

```
### 11. Ingrese un empleado con valor de legajo "112".
**solucion**
```sql
INSERT INTO empleados
VALUES (112, '30000111', 'Iván Ibarra');

```
### 12. Intente ingresar un registro empleando "currval":

```sql
insert into empleados
values (sec_legajoempleados.currval,'29000111','Hector Huerta');
```


### 13. Incremente el valor de la secuencia. Retorna 114.
**solucion**
```sql
SELECT sec_legajoempleados.nextval FROM dual;

```
### 14. Ingrese el registro del punto 11.

**solucion**
```sql
INSERT INTO empleados
VALUES (sec_legajoempleados.currval, '30000111', 'Iván Ibarra');

```

### 15. Recupere los registros.
**solucion**
```sql
SELECT * FROM empleados;

```
### 16. Vea las secuencias existentes y analice la información retornada.

**solucion**
```sql
SELECT sequence_name FROM user_sequences;

```
### 17. Vea todos los objetos de la base de datos actual que contengan en su nombre la cadena "EMPLEADOS".
**solucion**
```sql
SELECT object_name FROM user_objects WHERE object_name LIKE '%EMPLEADOS%';

```

### 18. Elimine la secuencia creada.
**solucion**
```sql
DROP SEQUENCE sec_legajoempleados;

```
### 19. Consulte todos los objetos de la base de datos que sean secuencias y verifique que "sec_legajoempleados" ya no existe.
**solucion**
```sql
SELECT sequence_name FROM user_sequences WHERE sequence_name = 'SEC_LEGAJOEMPLEADOS';

```