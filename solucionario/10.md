
# Ejercicios de laboratorio

Trabajamos con la tabla "libros" de una librería.

Eliminamos la tabla "libros":

```sql
drop table libros;
```

Creamos la tabla especificando que los campos "titulo" y "autor" no admitan valores nulos:

```sql
create table libros(
    titulo varchar2(30) not null,
    autor varchar2(30) not null,
    editorial varchar2(15) null,
    precio number(5,2)
);
```

Los campos "editorial" y "precio" si permiten valores nulos; el primero, porque lo especificamos colocando "null" en la definición del campo, el segundo lo asume por defecto.

Agregamos un registro a la tabla con valor nulo para el campo "precio":

```sql
insert into libros (titulo,autor,editorial,precio) values('El aleph','Borges','Emece',null);
```

Veamos cómo se almacenó el registro:

```sql
select *from libros;
```

No aparece ningún valor en la columna "precio".

Ingresamos otro registro, con valor nulo para el campo "editorial", campo que admite valores "null":

```sql
insert into libros (titulo,autor,editorial,precio) values('Alicia en el pais','Lewis Carroll',null,0);
```

Veamos cómo se almacenó el registro:

```sql
select *from libros;
```

No aparece ningún valor en la columna "editorial".

Ingresamos otro registro, con valor nulo para los dos campos que lo admiten:

```sql
insert into libros (titulo,autor,editorial,precio) values('Aprenda PHP','Mario Molina',null,null);
```

Veamos cómo se almacenó el registro:

```sql
select *from libros;
```

No aparece ningún valor en ambas columnas.

Veamos lo que sucede si intentamos ingresar el valor "null" en campos que no lo admiten, como "titulo":

```sql
insert into libros (titulo,autor,editorial,precio) values(null,'Borges','Siglo XXI',25);
```

Aparece un mensaje indicando que no se puede realizar una inserción "null" y la sentencia no se ejecuta.

Para ver cuáles campos admiten valores nulos y cuáles no, vemos la estructura de la tabla:

```sql
describe libros;
```

Nos muestra, en la columna "Null", que los campos "titulo" y "autor" están definidos "not null", es decir, no permiten valores nulos, los otros dos campos si los admiten.

Dijimos que la cadena vacía es interpretada como valor "null". Vamos a ingresar un registro con cadena vacía para el campo "editorial":

```sql
insert into libros (titulo,autor,editorial,precio) values('Uno','Richard Bach','',18.50);
```

Veamos cómo se almacenó el registro:

```sql
select *from libros;
```

No aparece ningún valor en la columna "editorial" del libro "Uno", almacenó "null".

Intentamos ingresar una cadena vacía en el campo "titulo":

```sql
insert into libros (titulo,autor,editorial,precio) values('','Richard Bach','Planeta',22);
```

Mensaje de error indicando que el campo no admite valores nulos.

Dijimos que una cadena de espacios NO es igual a una cadena vacía o valor "null". Vamos a ingresar un registro y en el campo "editorial" guardaremos una cadena de 3 espacios:

```sql
insert into libros (titulo,autor,editorial,precio)mvalues('Don quijote','Cervantes','   ',20);
```

Veamos cómo se almacenó el registro:

```sql
select *from libros;
```

Se muestra la cadena de espacios.

Recuperamos los registros que contengan en el campo "editorial" una cadena de 3 espacios:

```sql
select *from libros where editorial='   ';
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    titulo varchar2(30) not null,
    autor varchar2(30) not null,
    editorial varchar2(15) null,
    precio number(5,2)
);

insert into libros (titulo,autor,editorial,precio) values('El aleph','Borges','Emece',null);

select *from libros;

insert into libros (titulo,autor,editorial,precio) values('Alicia en el pais','Lewis Carroll',null,0);

select *from libros;

insert into libros (titulo,autor,editorial,precio) values('Aprenda PHP','Mario Molina',null,null);

select *from libros;

insert into libros (titulo,autor,editorial,precio) values(null,'Borges','Siglo XXI',25);

describe libros;

insert into libros (titulo,autor,editorial,precio) values('Uno','Richard Bach','',18.50);

select *from libros;

insert into libros (titulo,autor,editorial,precio) values('','Richard Bach','Planeta',22);

insert into libros (titulo,autor,editorial,precio) values('Don quijote','Cervantes','   ',20);

select *from libros;

select *from libros where editorial='   ';
```

# Ejercicios propuestos

## Ejercicio 01

Una farmacia guarda información referente a sus medicamentos en una tabla llamada "medicamentos".

### 1. Elimine la tabla y créela con la siguiente estructura:

```sql
 drop table medicamentos;
 create table medicamentos(
    codigo number(5) not null,
    nombre varchar2(20) not null,
    laboratorio varchar2(20),
    precio number(5,2),
    cantidad number(3,0) not null
);
```

### 2. Visualice la estructura de la tabla "medicamentos"
note que los campos "codigo", "nombre" y "cantidad", en la columna "Null" muestra "NOT NULL".
-----------
Consulta
```sql
describe medicamentos;
```

Salida
```sh
Nombre      ¿Nulo?   Tipo         
----------- -------- ------------ 
CODIGO      NOT NULL NUMBER(5)    
NOMBRE      NOT NULL VARCHAR2(20) 
LABORATORIO          VARCHAR2(20) 
PRECIO               NUMBER(5,2)  
CANTIDAD    NOT NULL NUMBER(3)
```

### 3. Ingrese algunos registros con valores "null" para los campos que lo admitan:

```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'Sertal gotas',null,null,100); 
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(2,'Sertal compuesto',null,8.90,150);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(3,'Buscapina','Roche',null,200);
```

### 4. Vea todos los registros.
Consulta
```sql
SELECT * FROM medicamentos;

```

Salida
```sh
CODIGO | NOMBRE            | LABORATORIO | PRECIO | CANTIDAD
-------+-------------------+-------------+--------+---------
1      | Sertal gotas      |             |        | 100     
2      | Sertal compuesto  |             | 8.90   | 150     
3      | Buscapina         | Roche       |        | 200

```

### 5. Ingrese un registro con valor "0" para el precio y cadena vacía para el laboratorio.
Consulta
```sql
INSERT INTO medicamentos (codigo, nombre, laboratorio, precio, cantidad) VALUES (4, 'Paracetamol', '', 0, 50);

```

Salida
```sh
1 fila insertada.

```

### 6. Intente ingresar un registro con cadena vacía para el nombre (mensaje de error)
Consulta
```sql
INSERT INTO medicamentos (codigo, nombre, laboratorio, precio, cantidad) VALUES (5, '', 'Roche', 15.50, 80);

```

Salida
```sh
Error que empieza en la línea: 183 del comando -
INSERT INTO medicamentos (codigo, nombre, laboratorio, precio, cantidad) VALUES (5, '', 'Roche', 15.50, 80)
Error en la línea de comandos : 183 Columna : 85
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("DANNER"."MEDICAMENTOS"."NOMBRE")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```

### 7. Intente ingresar un registro con valor nulo para un campo que no lo admite (aparece un mensaje de error)
Consulta
```sql
INSERT INTO medicamentos (codigo, nombre, laboratorio, precio, cantidad) VALUES (6, 'Ibuprofeno', 'Bayer', NULL, NULL);

```

Salida
```sh
Error que empieza en la línea: 188 del comando -
INSERT INTO medicamentos (codigo, nombre, laboratorio, precio, cantidad) VALUES (6, 'Ibuprofeno', 'Bayer', NULL, NULL)
Error en la línea de comandos : 188 Columna : 114
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("DANNER"."MEDICAMENTOS"."CANTIDAD")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```

### 8. Ingrese un registro con una cadena de 1 espacio para el laboratorio.
Consulta
```sql
INSERT INTO medicamentos (codigo, nombre, laboratorio, precio, cantidad) VALUES (7, 'Aspirina', ' ', 5.50, 200);

```

Salida
```sh
1 fila insertadas.
```

### 9. Recupere los registros cuyo laboratorio contenga 1 espacio (1 registro)
Consulta
```sql
SELECT * FROM medicamentos WHERE laboratorio LIKE ' ';

```

Salida
```sh
CODIGO | NOMBRE   | LABORATORIO | PRECIO | CANTIDAD
-------+----------+-------------+--------+---------
7      | Aspirina |             | 5.50   | 200

```

### 10. Recupere los registros cuyo laboratorio sea distinto de ' '(cadena de 1 espacio) (1 registro)
Consulta
```sql
SELECT * FROM medicamentos WHERE laboratorio <> ' ';

```

Salida
```sh
CODIGO | NOMBRE           | LABORATORIO | PRECIO | CANTIDAD
-------+------------------+-------------+--------+---------
1      | Sertal gotas     |             |        | 100

```

## Ejercicio 02

Trabaje con la tabla que almacena los datos sobre películas, llamada "peliculas".

### 1. Elimine la tabla:
```SQL
DROP TABLE PELICULAS;
```
### 2. Créela con la siguiente estructura:

```sql
create table peliculas(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    actor varchar2(20),
    duracion number(3)
);
```

### 3. Visualice la estructura de la tabla.

note que el campo "codigo" y "titulo", en la columna "Null" muestran "NOT NULL".
Consulta
```sql
DESCRIBE PELICULAS;
```

Salida
```sh
Nombre   ¿Nulo?   Tipo         
-------- -------- ------------ 
CODIGO   NOT NULL NUMBER(4)    
TITULO   NOT NULL VARCHAR2(40) 
ACTOR             VARCHAR2(20) 
DURACION          NUMBER(3) 
```

### 4. Ingrese los siguientes registros:

```sql
insert into peliculas (codigo,titulo,actor,duracion) values(1,'Mision imposible','Tom Cruise',120);
insert into peliculas (codigo,titulo,actor,duracion) values(2,'Harry Potter y la piedra filosofal',null,180);
insert into peliculas (codigo,titulo,actor,duracion) values(3,'Harry Potter y la camara secreta','Daniel R.',null);
insert into peliculas (codigo,titulo,actor,duracion) values(0,'Mision imposible 2','',150);
insert into peliculas (codigo,titulo,actor,duracion) values(4,'Titanic','L. Di Caprio',220);
insert into peliculas (codigo,titulo,actor,duracion) values(5,'Mujer bonita','R. Gere.J. Roberts',0);
```

### 5. Recupere todos los registros para ver cómo Oracle los almacenó.
Consulta
```sql

```

Salida
```sh
CODIGO |              TITULO                 |        ACTOR        | DURACION
-------+------------------------------------+---------------------+----------
   1   |        Mision imposible             |     Tom Cruise      |   120   
   2   | Harry Potter y la piedra filosofal  |         NULL        |   180   
   3   |  Harry Potter y la camara secreta   |      Daniel R.      |   NULL  
   0   |      Mision imposible 2              |                     |   150   
   4   |              Titanic                 |    L. Di Caprio     |   220   
   5   |         Mujer bonita                 | R. Gere.J. Roberts  |    0    

```

### 6. Intente ingresar un registro con valor nulo para campos que no lo admiten (aparece un mensaje de error)
Consulta
```sql
INSERT INTO peliculas (codigo, titulo, actor, duracion) VALUES (6, NULL, 'John Doe', 90);

```

Salida
```sh
Error que empieza en la línea: 214 del comando -
INSERT INTO peliculas (codigo, titulo, actor, duracion) VALUES (6, NULL, 'John Doe', 90)
Error en la línea de comandos : 214 Columna : 68
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("DANNER"."PELICULAS"."TITULO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.

```

### 7. Muestre todos los registros.
Consulta
```sql
SELECT * FROM peliculas;

```

Salida
```sh
+--------+------------------------------------+---------------------+----------+
| CODIGO |              TITULO                 |        ACTOR        | DURACION |
+--------+------------------------------------+---------------------+----------+
|   1    |        Mision imposible             |     Tom Cruise      |   120    |
|   2    | Harry Potter y la piedra filosofal  |         NULL        |   180    |
|   3    |  Harry Potter y la camara secreta   |      Daniel R.      |   NULL   |
|   0    |      Mision imposible 2              |                     |   150    |
|   4    |              Titanic                 |    L. Di Caprio     |   220    |
|   5    |         Mujer bonita                 | R. Gere.J. Roberts  |    0     |
+--------+------------------------------------+---------------------+----------+

```

### 8. Actualice la película en cuyo campo "duracion" hay 0 por "null" (1 registro)
Consulta
```sql
UPDATE peliculas SET duracion = NULL WHERE duracion = 0;


```

Salida
```sh

1 fila actualizadas.
```

### 9. Recupere todos los registros.

Consulta
```sql
SELECT * FROM peliculas;

```

Salida
```sh
CODIGO |           TITULO           |        ACTOR        | DURACION
-------+----------------------------+---------------------+----------
     1 | Mision imposible           | Tom Cruise          |      120
     2 | Harry Potter y la piedra...|                     |      180
     3 | Harry Potter y la camara...| Daniel R.           |         
     0 | Mision imposible 2         |                     |      150
     4 | Titanic                    | L. Di Caprio        |      220
     5 | Mujer bonita               | R. Gere.J. Roberts  |         

```

[11. Operadores relacionales (is null)](solucionario/11.md)